import {NextPage} from "next";

import {IPageProps} from "../components/@def";
import PageWrapper from "../components/PageWrapper";
import {AbstractPage} from "../components/AbstractPage";


const IndexPage: NextPage<IPageProps> = (props) => {
    return AbstractPage(props);
};

export default PageWrapper(IndexPage);
