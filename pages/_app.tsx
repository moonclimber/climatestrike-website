import React from "react";
import App from "next/app";

import NextI18NextInstance from "../core/I18n";


class MyApp extends App {
    render() {
        const {Component, pageProps} = this.props;
        return (
            <Component {...pageProps} />
        );
    }
}

export default NextI18NextInstance.appWithTranslation(MyApp);