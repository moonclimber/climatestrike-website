import {IConfiguration} from "../interfaces/config";
import {ELinkLocation} from "../interfaces/model";

const socialConfig = [
    {
        id: "instagram",
        url: "https://www.instagram.com/klimastreikschweiz/"
    },
    {
        id: "facebook",
        url: "https://www.facebook.com/klimastreikschweiz/"
    },
    {
        id: "twitter",
        url: "https://twitter.com/klimastreik"
    },
    {
        id: "youtube",
        url: "https://www.youtube.com/channel/UC_caP9IpnSQ_t6KTAOBVqYA"
    }
];

export const config: IConfiguration = {
    dataSources: {
        strategy: "mock",
        config: {
            url: "http://localhost:1337"
        }
    },
    menu: {
        contributions: [
            {
                strategy: "icon-menu-item",
                config: {
                    src: "/static/images/logo-new.svg",
                    href: "/",
                }
            },
            {
                strategy: "menu-item",
                config: {
                    location: ELinkLocation.header
                }
            }
        ]
    },
    submenu: {
        contributions: [
            {
                strategy: "social",
                config: {
                    social: socialConfig
                }
            },
            {
                strategy: "languages",
                config: {
                    languages: [
                        {id: "de", name: "DE"},
                        {id: "en", name: "EN"},
                        {id: "fr", name: "FR"},
                        {id: "it", name: "IT"}
                    ]
                }
            }
        ]
    },
    footer: {
        contributions: [
            {
                strategy: "link-item",
                config: {
                    nColumns: 2,
                    location: ELinkLocation.footer
                }
            },
            {
                strategy: "social",
                config: {
                    social: socialConfig
                }
            },
            {
                strategy: "form-dialog",
                config: {
                    submitText: "Register for newsletter",
                    submitUrl: "/register-newsletter",
                    fields: [
                        {key: "email", type: "email", typeRule: "email", required: true, placeholder: "Email", icon: "mail", hideBeforeFocus: false},
                        {key: "zip", type: "text", typeRule: "regex", pattern: "[0-9]{4}", required: true, placeholder: "ZIP", icon: "home", hideBeforeFocus: true},
                        {key: "firstname", type: "text", typeRule: "string", required: false, placeholder: "First name", icon: "user", hideBeforeFocus: true},
                        {key: "lastname", type: "text", typeRule: "string", required: false, placeholder: "Last name", icon: "team", hideBeforeFocus: true}
                    ]
                }
            },
        ]
    },
    languages: {
        languages: ["de", "en", "fr", "it"],
        default: "en"
    },
    colors: {
        default: [
            "#FF71A6",
            "#FA6900BA",
            "#8A8ECD",
            "#1693A5",
            "#C8D06D",
        ]
    }
};