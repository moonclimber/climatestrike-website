export interface ICacheOptions {
    enabled: boolean;
    max?: number; // Max number of entries
    maxAge?: number; // Maximum age in ms
}

export interface ICache {
    get<T>(key: string, provider: () => Promise<T>): Promise<T>;
    clear(): void;
}