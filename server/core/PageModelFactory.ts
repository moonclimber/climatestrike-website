// import {MockAPI} from "../api/MockAPI";
import {IPageModelStrategy} from "../../interfaces/model";
import {StrapiAPI} from "../api/StrapiAPI";

export class PageModelFactory {
    public create(type: string, config: any): IPageModelStrategy {
        switch (type) {
            // case "mock":
            //     return new MockAPI();
            case "strapi":
                return new StrapiAPI(config);
            default:
                throw new Error(`PageModelFactory - Cannot create type '${type}'`);
        }
    }
}