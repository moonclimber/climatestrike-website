host: api.climatestrike.ch

# climatestrike events

The climatestrike events api provides access to climatestrike events

## List Events [GET /events?amount=<1..20>&offset=<offset>&id=<id>&type=<type>]

+ Response 200 (application/json)


        [
            {
                "id": Number,
                "type": String
                "title": {
                    "de": String,
                    "fr": String,
                    "it": String,
                    "en": String
                },
                "description": {
                    "de": String,
                    "fr": String,
                    "it": String,
                    "en": String
                },
                "events": [
                    {
                        "description": String
                        "location": String,
                        "lat": String,
                        "lon": String,
                        "start_time": String,
                        "end_time": String
                    },
                    ...
                ]
            },
            ...
        ]


### Example

        [
            {
                "id": "1"
                "title": {
                    "de": "Am Boden bleiben",
                    "fr": "Rester à terre",
                    "it": "Rimanere a terra",
                    "en": "Stay grounded"
                },
                "description": {
                    "de": "Am Boden bleiben - dein Neujahrsvorsatz f\u00FCr 2020\r\n\r\n\u2139\uFE0F Informier dich hier \u00FCber Flugverkehr und Klimaschutz:\r\n\r\nwww.flugfacts.ch\r\n\r\n\uD83D\uDCAA und trag dich jetzt beim Flugstreik ein, um 2020 am Boden zu bleiben:\r\n\r\nwww.flugstreik.earth\r\n\r\n\uD83D\uDC49 Gestalte dein Transpi mit dem Text:\r\n\r\n\"Ich bleib am Boden\"\r\n\r\n\uD83D\uDC49 Mach ein Foto von dir mit dem Transpi und poste es ab dem 01.01.20 auf Facebook und deinen anderen sozialen Kan\u00E4len mit dem Vermerk @Flugstreik und den Hashtags #Flugstreik #flugfacts #ZugstattFlug #AmBodenbleiben",
                    "fr": "Rester \u00E0 terre - ta r\u00E9solution du Nouvel An pour 2020\r\n\r\n\u2139\uFE0F Pour en savoir plus sur le transport a\u00E9rien et la protection du climat, clique ici :\r\n\r\nwww.aviation-verite.ch\r\n\r\n\uD83D\uDCAA et inscris-toi d\u00E8s maintenant pour la gr\u00E8ve de l'avion pour rester \u00E0 terre en 2020 :\r\n\r\nwww.flightstrike.earth\r\n\r\n\uD83D\uDC49 Con\u00E7ois ta banni\u00E8re avec le texte :\r\n\r\n\"Je reste \u00E0 terre\"\r\n\r\n\uD83D\uDC49 Prends une photo de toi avec une banni\u00E8re et affiche-la \u00E0 partir du 01.01.20 sur Facebook et tes autres canaux sociaux avec la note @Flugstreik et les hashtags #Flugstreik #flugfacts #ZugstattFlug #resteraterre",
                    "it": "Rimanete a terra - la vostra risoluzione di Capodanno per il 2020\r\n\r\n\u2139\uFE0F Qui trovate maggiori informazioni sull'aviazione e sulla protezione del clima:\r\n\r\nwww.flugfacts.ch\r\n\r\n\uD83D\uDCAA e iscrivetevi subito al volo per rimanere a terra nel 2020:\r\n\r\nwww.flugstreik.earth\r\n\r\n\uD83D\uDC49 Progetta il tuo banner con il testo:\r\n\r\n\"Rimarr\u00F2 con i piedi per terra\"\r\n\r\n\uD83D\uDC49 Scattatevi una foto con il banner e postatela dal 01.01.20 in poi su Facebook e sugli altri canali sociali con la nota @Flugstreik e gli hashtag #Flugstreik #Flugstreik #flugfacts #ZugstattFlug #Rimaneteaterra",
                    "en": "Stay Grounded - your New Year's resolution for 2020\r\n\r\n\u2139\uFE0F Find out more about aviation and climate protection here:\r\n\r\nwww.flugfacts.ch\r\n\r\n\uD83D\uDCAA and sign up now for the flightstrike to stay grounded in 2020:\r\n\r\nwww.flugstreik.earth\r\n\r\n\uD83D\uDC49 Design your banner with the text:\r\n\r\n\"I will stay grounded\"\r\n\r\n\uD83D\uDC49 Take a picture of yourself with the banner and post it from 01.01.20 onwards on Facebook and your other social channels with the note @Flugstreik and the hashtags #Flugstreik #flugfacts #ZugstattFlug #StayGrounded"
                },
                "events": [
                    {
                        "description": Null
                        "location": Null,
                        "lat": Null,
                        "lon": Null,
                        "start_time": "2020-01-00T00:00:00.000Z",
                        "end_time": "2020-12-29T00:00:00.000Z"
                    }
                ]
            }
        ]
