import {map, sortBy} from "lodash";

import {HttpService} from "../../core/HttpService";
import {IEventGroup} from "../../components/@def";


interface IFacebookEventNode {
    id: string; // e.g. "483965702230779",
    startTimestampForDisplay: number;
    time_range: {
        start: string; // e.g. "2020-01-14T19:00:00+0100"
        end?: string;
    },
    timezone: string; // e.g. "Europe/Zurich",
    happensOnSingleDay: boolean,
    childEvents: {
        count: number;
    },
    is_event_draft: boolean;
    scheduled_publish_timestamp: number;
    shortTimeLabel: string; // e.g. "Tue 7:00 PM UTC+01",
    shortDateLabel: string; // e.g. "Tue, Jan 14",
    suggested_event_context_sentence: {
        text: string; // e.g. "53 guests"
    },
    event_place: {
        __typename: string; // e.g. "FreeformPlace",
        contextual_name: string; // e.g. "Hardstrasse 235, 8005 Zürich Zürich, Schweiz",
        id: string; // e.g. "1535743156573429"
    },
    is_canceled: boolean;
    imported_source_name: any,
    preassigned_discount_note: any,
    name: string; // e.g. "Workers for future meeting - ClimateEventZürich",
    has_child_events: boolean;
    event_buy_ticket_url: any,
    can_viewer_purchase_onsite_tickets: boolean;
    event_viewer_capability: {
        is_viewer_admin_no_business_permissions: boolean;
        can_viewer_edit: boolean;
        can_viewer_share: boolean;
        can_viewer_create_repeat_event: boolean;
        canViewerWatch: boolean;
    },
    tickets_type: string; // e.g. "ONSITE_TICKET",
    canViewerJoin: boolean;
    isPast: boolean;
    connectionStyle: string; // e.g. "INTERESTED",
    eventID: string; // e.g. "483965702230779",
    watchStatus: string; // e.g. "UNWATCHED",
    canJoin: boolean;
    __typename: string; // e.g. "Event"
}

interface IFacebookEvent {
    is_hidden_on_profile_calendar: boolean;
    is_added_to_profile_calendar: boolean;
    node: IFacebookEventNode;
    cursor: string;
}


export class FacebookAPI {
    private $http = new HttpService();

    constructor() {}

    public async getUpcomingEvents(): Promise<IEventGroup[]> {
        const response = await this.$http.postd<any>(
            "https://www.facebook.com/api/graphql/",
            "&variables=%7B%22pageID%22%3A%221227679007379847%22%2C%22count%22%3A90%2C%22cursor%22%3A%22AQHRsQ7j7HQHBniQBMNTKOl0kI3jXJ631B3hWBvayrAdlsQMK_m3tLRXWAc3NUgvcJXJU6ks3F_nVD-n_JHSmgs59g%22%7D&doc_id=2464276676984576");
        const events = response.data.page.upcoming_events.edges;
        return this.mapEvents(events);
    }

    public async getPastEvents(): Promise<IEventGroup[]> {
        const response = await this.$http.postd<any>(
            "https://www.facebook.com/api/graphql/",
            "&variables=%7B%22pageID%22%3A%221227679007379847%22%7D&doc_id=2620926351263365");
        const events = response.data.page.past_events.edges;
        return this.mapEvents(events);

    }

    private mapEvents(events): IEventGroup[] {
        return sortBy(map(events, (event: IFacebookEvent) => {
            const node = event.node;
            return {
                id: node.id,
                title: {de: node.name},
                startTime: node.time_range.start,
                endTime: node.time_range.end,
                description: null,
                categories: [],
                events: [
                    {
                        description: null,
                        startTime: node.time_range.start,
                        endTime: node.time_range.end,
                        location: node.event_place.contextual_name,
                        zip: null
                    }
                ]
            };
        }), (dp) => dp.startTime);
    }
}