import NextI18Next from "next-i18next";
import {config} from "../config/config";
import {reject, keyBy} from "lodash";
import {ILanguageSwitch} from "../components/@def";

const languageConfig = config.languages;
const NextI18NextInstance = new NextI18Next({
    defaultLanguage: languageConfig.default,
    otherLanguages: reject(languageConfig.languages, language => language == languageConfig.default),
    localeSubpaths: languageConfig.paths ? languageConfig.paths : keyBy(languageConfig.languages)
});

export default NextI18NextInstance;

/* Optionally, export class methods as named exports */
export const {
    appWithTranslation,
    withTranslation,
    Link
} = NextI18NextInstance;

export const getLanguageText = (obj: ILanguageSwitch, languageId: any) => {
    return obj[languageId] as string;
};