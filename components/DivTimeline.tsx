import React, {useEffect, createRef} from "react";
import moment from "moment";
import classNames from "classnames";
import {Grid} from "@material-ui/core";

import {IEventsProps, IPageContextProps, II18nProps, IEventDialogProps, EButtonStyle} from "./@def";
import {withTranslation} from "../core/I18n";
import {findIndex, findLastIndex, map} from "lodash";
import Button from "./Button";


const _Timeline: React.FC<IEventsProps & IPageContextProps & IEventDialogProps & II18nProps> = (props) => {
    const {context, items, setDialogData} = props;
    const ref = createRef();
    const className = classNames("div-timeline", context.itemId);
    const now = moment();
    const lastBefore = findLastIndex(items, item => moment(item.startTime).isSameOrBefore(now, "day"));
    const firstAfter = findIndex(items, item => moment(item.startTime).isSameOrAfter(now, "day"));
    const nowIndex = (lastBefore + firstAfter) / 2;

    // Scroll to today: https://www.robinwieruch.de/react-scroll-to-item
    useEffect(
        () => (ref.current as any).scrollIntoView(),
        [ref]);
    return (
        <div className={className}>
            {map(items, (item, index) => {
                const startMoment = moment(item.startTime);
                return <Grid key={index} className="timeline-event" wrap="nowrap" justify="space-between" alignItems="stretch" container >
                    <div ref={index == firstAfter ? ref as any : null} className="timeline-date" style={{width: 75}}>
                        <div className="timeline-day">{startMoment.format("DD.")}</div>
                        <div className="timeline-month">{startMoment.format("MMM")}</div>
                    </div>
                    <div className={classNames("timeline-circle-container", {"before-today": index < nowIndex, "after-today": index > nowIndex, today: nowIndex === index})}>
                        <div className={classNames("timeline-circle")}>
                        </div>
                    </div>
                    <div style={{width: "70%"}}>
                        <h1>{item.title}</h1>
                        {item.description && <p>{item.description}</p>}
                        {item.events.length && <p>{map(item.events, (event) => event.location).join(", ")}</p>}
                        <Button style={EButtonStyle.primary} type="more" onClick={() => setDialogData(item)}></Button>
                        {/*FIXME: Super hacky spacing to not mess up background-color (line)*/}
                        {index !== items.length - 1 && <div style={{height: "5rem"}} />}
                    </div>
                </Grid>;
            })}
        </div>
    );
};

export const DivTimeline = withTranslation()(_Timeline);