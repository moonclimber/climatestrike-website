import React, {useRef, useEffect, useState} from "react";
import moment from "moment";
import * as d3 from "d3";
import classNames from "classnames";

import {IEventsProps, IPageContextProps, II18nProps, IEventGroup, IEventDialogProps} from "./@def";
import {withTranslation, getLanguageText} from "../core/I18n";
import {cloneDeep, findIndex, findLastIndex, map} from "lodash";


// https://stackoverflow.com/questions/14167863/how-can-i-bring-a-circle-to-the-front-with-d3
d3.selection.prototype.moveToBack = function () {
    return this.each(function () {
        var firstChild = this.parentNode.firstChild;
        if (firstChild) {
            this.parentNode.insertBefore(this, firstChild);
        }
    });
};

// TODO: Refactor 
// TODO: Add a way to dynamically refetch data
const _Timeline: React.FC<IEventsProps & IPageContextProps & IEventDialogProps & II18nProps> = (props) => {
    const height = 600;
    const width = "100%";
    const xPos = 150;
    const margin = {top: 0, bottom: 0, left: 0, right: 0};
    const maxItems = 4;
    const itemsScroll = 2;
    const {context, items, setDialogData, t} = props;
    const d3Container = useRef(null);
    const [k, setK] = useState(1);
    const now = moment();
    const lastBefore = findLastIndex(items, item => moment(item.startTime).isSameOrBefore(now, "day"));
    const firstAfter = findIndex(items, item => moment(item.startTime).isSameOrAfter(now, "day"));
    const nowIndex = (lastBefore + firstAfter) / 2;
    const pos = nowIndex - 0.5;
    const [positions, setPositions] = useState({current: pos, previous: pos});

    useEffect(
        () => {
            if (items && d3Container.current) {


                const scale = d3.scaleLinear().domain([0, maxItems]).range([0 + margin.top, height - margin.bottom]);
                const svg = d3.select(d3Container.current);

                var pan = d3.zoom()
                    .on("zoom", function () {
                        const e = d3.event;
                        let newPositions = cloneDeep(positions);
                        newPositions.previous = positions.current;
                        if (e.transform.k < k) {
                            newPositions.current = positions.current + itemsScroll;
                        }
                        else {
                            newPositions.current = positions.current - itemsScroll;
                        }
                        setPositions(newPositions);
                        setK(e.transform.k);
                    });
                pan.filter(() => {
                    // scroll down
                    if (d3.event.deltaY > 0) {
                        const result = positions.current + itemsScroll < items.length - maxItems / 2;
                        // next scroll does ends up with half of the space filled
                        return result;
                    }
                    else {
                        // scroll up
                        const result = positions.current > 0;
                        // next scroll does ends up with half of the space filled
                        return result;
                    }
                });
                svg.call(pan);

                const line = svg.selectAll(".line")
                    //  The line behaves strangely if no id-function is given. 
                    .data([0, 1], (d) => "" + d);

                line.enter()
                    .append("line")
                    .attr("x1", xPos)
                    .attr("x2", xPos)
                    .classed("line", true);

                line.exit().remove();

                const lines = svg.selectAll(".line");
                (lines as any).moveToBack();

                const offset = nowIndex - positions.current;
                svg.selectAll(".line")
                    .attr("class", (d) => classNames("line", d === 0 ? "before-today" : "after-today"))
                    .transition()
                    .attr("y1", (d) => d === 0 ? scale(0) : scale(offset))
                    .attr("y2", (d) => d === 0 ? scale(offset) : scale(maxItems));


                // Bind D3 data
                const groups = svg
                    .selectAll(".container")
                    .data(items, (d: any) => d.id);

                // Enter new D3 elements
                const enter = groups.enter()
                    .append("g");

                // Remove old D3 elements
                groups.exit()
                    .remove()
                    .transition();

                enter
                    .classed("container", true);

                const dateGroup = enter.append("g");
                dateGroup.classed("date-container", true)
                    .attr("transform", "translate(-80)");
                dateGroup.append("text")
                    .classed("date-day", true)
                    .attr("dy", "0em")
                    .text((d: IEventGroup) => moment(d.startTime).format("DD."));
                dateGroup.append("text")
                    .classed("date-month", true)
                    .attr("dy", "1em")
                    .text((d: IEventGroup) => moment(d.startTime).format("MMM"));

                const infoGroup = enter.append("g");
                infoGroup.classed("info-container", true)
                    .attr("transform", "translate(50)");
                infoGroup.append("text")
                    .attr("dy", "0em")
                    .classed("title", true)
                    .text((d: IEventGroup) => getLanguageText(d.title, props.i18n.language));
                infoGroup.append("text")
                    .attr("dy", "1em")
                    .classed("description", true)
                    .text((d: IEventGroup) => d.description ? getLanguageText(d.description, props.i18n.language) : map(d.events, (event) => event.location).join(", "));
                infoGroup.append("text")
                    .attr("dy", "2em")
                    .classed("button", true)
                    .text(t("more"))
                    .on("click", (d) => {
                        setDialogData(d);
                    });

                enter.append("circle")
                    .attr("r", 28.5);

                svg.selectAll(".container")
                    .attr("transform", `translate(${xPos})`)
                    .transition()
                    .attrTween("transform", (_d: any, i) => {
                        return (t) => {
                            const loc = i - ((1 - t) * positions.previous + t * positions.current);
                            return `translate(${xPos}, ${scale(loc)})`;
                        };
                    });
            }
        },
        [items, k, margin.bottom, margin.top, t, positions, setDialogData]
    );

    const className = classNames("timeline", context.itemId);
    return (
        <div className={className}>
            <svg className="timeline-svg"
                width={width}
                height={height}
                ref={d3Container}
            />
        </div>
    );
};

export const Timeline = withTranslation()(_Timeline);