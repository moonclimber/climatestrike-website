import * as React from "react";


interface IImageProps {
    title?: string;
    src: string;
    color?: string;
    caption?: string;
    link?: string;
}

const Image: React.FunctionComponent<IImageProps> = (props) => {
    const {src} = props;
    return <div className="home-item" style={{backgroundImage: src}}></div>;
};

export default Image;
