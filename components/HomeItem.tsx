import * as React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {IHomeItemProps, IPageContextProps, II18nProps, EButtonStyle} from "./@def";
import {renderTitle, renderSubtitle, renderDescription, renderLink} from "./BaseComponents";
import {withTranslation, getLanguageText} from "../core/I18n";


// TODO: Better naming
const _HomeItem: React.FunctionComponent<IHomeItemProps & IPageContextProps & II18nProps> = (props) => {
    const {title, subtitle, description, image, link, i18n} = props;
    const className = classNames("home-item-content", "row-of-" + props.context.rowItems.length, props.context.itemId);
    const color = image ? image.color : null;
    const style = {
        backgroundImage: image
            && `linear-gradient(${color}, ${color}), url('${image.src}')`,
    };
    const renderers = [];
    if (title) renderers.push(() => renderTitle(getLanguageText(title, i18n.language)));
    if (subtitle) renderers.push(() => renderSubtitle(getLanguageText(subtitle, i18n.language)));
    if (description) renderers.push(() => renderDescription(getLanguageText(description, i18n.language)));
    if (link) renderers.push(() => renderLink({style: EButtonStyle.primary, href: link["link_" + i18n.language]}));

    return <div
        className="home-item"
        style={style} >
        <div className={className}>
            {map(renderers, renderer => renderer())}

        </div>
    </div >;
};

export const HomeItem = withTranslation()(_HomeItem);
