import React from "react";
import classNames from "classnames";
import {map} from "lodash";

import {renderLink} from "./BaseComponents";
import {IImgProps, IPageContextProps, II18nProps, EButtonStyle} from "./@def";
import {withTranslation, getLanguageText} from "../core/I18n";


export const _Img: React.FC<IImgProps & IPageContextProps & II18nProps> = ({image, link, context, i18n}) => {
    const className = classNames("image", context.itemId);
    const src = getLanguageText(image.src, i18n.language);
    const renderers = [
        () => <img key="img" src={src} />
    ];
    const languageLink = link["link_" + i18n.language];
    if (languageLink) renderers.push(() => renderLink({style: EButtonStyle.primary, href: languageLink}));
    return <div className={className} >
        {map(renderers, renderer => renderer())}
    </div>;
};

export const Img = withTranslation()(_Img);