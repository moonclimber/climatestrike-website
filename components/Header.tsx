import React, {useReducer, useState, useEffect} from "react";
import {AppBar, Toolbar, Drawer} from "@material-ui/core";
import {find, map, reject} from "lodash";

import {DetectionService} from "../core/DetectionService";
import {IContribution, IHeaderProps} from "./@def";
import {Submenu} from "./Submenu";
import {ICON_MENU_ITEM} from "../config/consts";
import {IconFactory} from "../factories/IconFactory";
import classNames from "classnames";
import {isBoolean} from "util";


interface ILayoutProps {
    isMobile: boolean;
    activePageIdentifier: string;
    menuContributions: IContribution[];
    submenuContributions: IContribution[];
    footerContributions: IContribution[];
}

export const Header: React.FC<ILayoutProps> = (props) => {
    const isSmall = DetectionService.instance().isSmall();
    const useSmall = isBoolean(isSmall) ? isSmall : props.isMobile;
    const childProps = {useSmall, ...props};
    return <AppBar className="header" position="fixed" hidden={false}>
        <Toolbar className="toolbar">
            <DesktopHeader {...childProps} />
            <MobileHeader {...childProps} />
        </Toolbar>
    </AppBar >;
};

const DesktopHeader = (props: IHeaderProps) => {
    const {useSmall, menuContributions, submenuContributions} = props;
    const isHidden = () => window.pageYOffset > 0;
    const [hideSubmenu, setHideSubmenu] = useState(false);
    const scrollListener = () => {
        const hide = isHidden();
        setHideSubmenu(hide);
    };
    useEffect(
        () => {
            window.addEventListener("scroll", scrollListener);
            return () => {window.removeEventListener("scroll", scrollListener);};
        },
        []);
    return < div className={classNames("header", "desktop")} style={{display: useSmall ? "none" : "block"}}>
        <div className={classNames("submenu-container", {hidden: hideSubmenu})}>
            <Submenu className={classNames("desktop")} contributions={submenuContributions} />
            <hr className="submenu-separator"></hr>
        </div>
        <div className={classNames("menu", "navigation", "desktop")}>
            {map(menuContributions, contribution => contribution.provider())}
        </div>
    </div >;
};

// FIXME toggleDrawer call without parameter?
const MobileHeader = (props: IHeaderProps) => {
    const {useSmall, menuContributions, submenuContributions} = props;
    const [showDrawer, toggleDrawer] = useReducer((state) => {return !state;}, false);
    const iconFactory = new IconFactory();
    const iconContribution = find(menuContributions, contribution => contribution.identifier === ICON_MENU_ITEM);
    const contributions = reject(menuContributions, iconContribution);
    return <div className={classNames("header", "mobile")} style={{display: useSmall ? "block" : "none"}}>
        <div className={classNames("menu", "mobile")}>
            {iconContribution && iconContribution.provider()}
            <div onClick={() => toggleDrawer(null)}>{iconFactory.create("menu")}</div>
        </div>
        <Drawer
            anchor="right"
            open={showDrawer}
            classes={{root: "drawer", paperAnchorRight: "drawer-right"}}
            onClose={() => toggleDrawer(null)}
        >
            <div className={classNames("navigation", "mobile")}>
                {map(contributions, contribution => contribution.provider())}
            </div>
            <Submenu className="mobile" contributions={submenuContributions} />
        </Drawer>
    </div>;
};