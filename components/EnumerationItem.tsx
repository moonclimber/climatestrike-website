import React from "react";
import classNames from "classnames";
import {map, filter, findIndex} from "lodash";
import {Grid} from "@material-ui/core";

import {TObjectId} from "../interfaces/core";
import {IContentItem} from "../interfaces/model";
import {IEnumerationItemProps, IPageContextProps} from "./@def";
import {renderDescription, renderLink, renderImage, renderEnumeration} from "./BaseComponents";
import {DetectionService} from "../core/DetectionService";


const getEnumeration = (itemId: TObjectId, items: IContentItem[]) => {
    const enumItems = filter(items, item => item.type == "enumeration-item");
    const itemIndex = findIndex(enumItems, item => item.identifier == itemId);
    return itemIndex + 1;
};

const EnumerationContent = ({n, title, description, link}) => {
    const renderers = [];
    renderers.push(() => <span key="enumeration-container">
        {renderEnumeration(n)}
        <span key="enumeration-title" className="enumeration-title">{title}</span>
    </span>);
    if (description) renderers.push(() => renderDescription(description));
    if (link) renderers.push(() => renderLink(link));
    return <div className="enumeration-content">
        {map(renderers, renderer => renderer())}
    </div>;
};

export const EnumerationItem: React.FC<IEnumerationItemProps & IPageContextProps> = (props) => {
    const detection = DetectionService.instance();
    const {context, title, description, image, link} = props;
    const className = classNames("enumeration-item", context.itemId);
    const renderers = [() => <Grid item key="image" xs={12} sm={4}>{renderImage(image)}</Grid>];
    const enumeration = getEnumeration(context.itemId, context.items);
    const renderContent = () => <Grid item key="content" xs={12} sm={8}><EnumerationContent n={enumeration} title={title} description={description} link={link} /></Grid>;
    if (enumeration % 2 === 0 || process.browser && detection.isSmall()) {
        renderers.unshift(renderContent);
    }
    else {
        renderers.push(renderContent);
    }
    return <Grid className={className} container spacing={3}>
        {map(renderers, renderer => renderer())}
    </Grid>;
};