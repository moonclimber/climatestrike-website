import {TObjectId, IFormField} from "../interfaces/core";
import {IPage, IContentItem} from "../interfaces/model";
import {IMenuConfig, ISubMenuConfig, IFooterConfig} from "../interfaces/config";
import {WithTranslation} from "next-i18next";


export interface IAppContext {
    activePageIdentifier: TObjectId;
    pages: IPage[];
}

export interface IContribution {
    identifier: TObjectId;
    provider: () => React.ReactNode;
}

export interface ISubMenuProps {
    className: string;
    contributions: IContribution[]
}

export interface IHeaderProps {
    useSmall: boolean;
    menuContributions: IContribution[];
    submenuContributions: IContribution[];
}

export interface IFooterProps {
    contributions: IContribution[]
}

export interface IAbstractPageProps {
    activePageIdentifier: string;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageProps {
    activePageIdentifier: TObjectId;
    pages: IPage[];
    items: IContentItem[];
}

export interface IPageWrapperProps extends IPageProps {
    title?: string;
    isMobile: boolean;
    pages: IPage[];
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    children?: React.ReactNode;
}

export interface IPageContext {
    itemId: TObjectId;
    identifier: string;
    items: IContentItem[];
    rowItems: IContentItem[];
}

export interface IPageContextProps {
    context: IPageContext;
}

export interface II18nProps extends WithTranslation {
}

export interface IFormProps {
    submitText: string;
    submitUrl: string;
    fields: IFormField[];
}

export interface ILanguageSwitch {
    de?: string;
    en?: string;
    fr?: string;
    it?: string;
}

export enum EButtonStyle {
    primary = "primary",
    secondary = "secondary"
}

export interface IButtonProps {
    style: EButtonStyle;
    type?: string;
    text?: ILanguageSwitch;
    href?: string;
    onClick?: () => void;
}

export interface IImageConfig {
    identifier: string;
    src: ILanguageSwitch;
    position: "background" | "left" | "right" | "alternating"; // Not yet supported
    color?: string;
    caption?: ILanguageSwitch;
}

interface ILinkConfig {
    identifier: string;
    link_de: string;
    link_en: string;
    link_fr: string;
    link_it: string;
    text_de: string;
    text_en: string;
    text_fr: string;
    text_it: string;
    type?: string;
    text?: ILanguageSwitch;
}

export interface IHomeItemProps {
    title?: ILanguageSwitch;
    subtitle?: ILanguageSwitch;
    description?: ILanguageSwitch;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface IImgProps {
    image: IImageConfig;
    link?: ILinkConfig;
}

export interface IRichTextProps {
    html: string;
}

export interface IIframeProps {
    src: string;
}

export interface ITileItemProps {
    title: ILanguageSwitch;
    subtitle: ILanguageSwitch;
    description: ILanguageSwitch;
    icon: IImageConfig;
    image: IImageConfig;
}

export interface IEnumerationItemProps {
    title: ILanguageSwitch;
    description?: ILanguageSwitch;
    image?: IImageConfig;
    link?: ILinkConfig;
}

export interface ILinkSectionProps {
    title: ILanguageSwitch;
    description: ILanguageSwitch;
    link: ILinkConfig;
}

interface IEvent {
    description: ILanguageSwitch;
    startTime: string;
    endTime: string;
    location: string;
    zip: number;
}

export interface IEventGroup {
    title: ILanguageSwitch;
    startTime: string;
    endTime: string;
    description: ILanguageSwitch;
    categories: string[];
    events: IEvent[];
}

export interface IEventsProps {
    items: IEventGroup[];
}

export interface IEventDialogProps {
    setDialogData: (data: IEventGroup) => void;
}

export interface IMapProps {
    mapData: any; // Some freaky JSON
    contentItems: IContentItem<any>[];
}

type TContactType = "website" |
    "email" |
    "telegram" |
    "whatsapp" |
    "facebook" |
    "instagram" |
    "discord" |
    "other"

interface IContactItem {
    type: TContactType;
    link: string;
    linkName?: ILanguageSwitch;
}

export interface IContactGroup {
    identifiers: TObjectId[];
    description?: ILanguageSwitch;
    default?: boolean;
    content: IContactItem[];
}

export interface IContactDataProps {
    selection: TObjectId[];
    items: IContactGroup[];
}
