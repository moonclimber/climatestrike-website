import React from "react";
import {map, includes} from "lodash";

import {TObjectId, INamedObject} from "../interfaces/core";
import classNames from "classnames";
import {withTranslation} from "../core/I18n";
import {II18nProps} from "./@def";


interface ISelectorProps {
    items: INamedObject[];
    selected?: TObjectId[];
    onClick?: (id: TObjectId) => void;
    className?: string;
}

export const Selector: React.FC<ISelectorProps> = ({items, selected, onClick, className}) => {
    return <div className={classNames("selector", className)}>
        {map(items, (item) => {
            const active = includes(selected, item.id);
            const className = classNames("selector-item", {selected: active});
            return <div key={item.id} className={className} onClick={() => onClick && onClick(item.id)}>{item.name}</div>;
        })}
    </div >;
};

export const LanguageSelector = withTranslation()<ISelectorProps & II18nProps>(({items, i18n}) => <Selector
    items={items}
    selected={[i18n.language]}
    onClick={(id) => i18n.changeLanguage(id)}
/>);